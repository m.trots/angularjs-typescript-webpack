import { NgModule } from "angular-ts-decorators";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TodoDetailComponent } from "./todo-detail/todo-detail.component";
import { TodoService } from "./todo.service";
import { TodoListComponent } from "./todo-list/todo-list.component";
import { PushService } from "./push.service";

@NgModule({
  id: "AppModule",
  imports: [AppRoutingModule],
  declarations: [AppComponent, TodoListComponent, TodoDetailComponent],
  providers: [TodoService, PushService],
  bootstrap: [AppComponent]
})
export class AppModule {
  static run(pushService: PushService) {
    "ngInject";
    document.addEventListener("deviceready", () => {
      pushService.registartion();
    });
  }
}
