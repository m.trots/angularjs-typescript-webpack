export class Todo {
  id: number;
  name?: string;
  title?: string;
  done?: boolean;
  description?: string;
  registrationId?: string;
  time?: string;
}
