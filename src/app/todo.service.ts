import { IHttpService, IPromise, IQService } from "angular";
import { Injectable } from "angular-ts-decorators";
import { Todo } from "./todo";
import { PushService } from "./push.service";

@Injectable("todoService")
export class TodoService {
  private todos: Todo[] = [];
  private TODO_API = "http://10.10.2.96:4100";

  /*@ngInject*/
  constructor(
    private $http: IHttpService,
    private $q: IQService,
    private pushService: PushService
  ) {}

  /** GET todo-list from the server */
  getTodos(): IPromise<Todo[]> {
    const deferred = this.$q.defer<Todo[]>();
    if (this.todos.length) {
      deferred.resolve(this.todos);
    } else {
      this.$http.get<Todo[]>(this.TODO_API).then(
        response => {
          // this.log('fetched todo-list');
          this.todos = response.data;
          deferred.resolve(response.data);
        },
        error => {
          // this.log(error);
          deferred.reject(error);
        }
      );
    }
    return deferred.promise;
  }

  /** GET todo by id */
  getTodo(id): IPromise<Todo> {
    const deferred = this.$q.defer<Todo>();
    const todo: Todo = this.todos.find(h => h.id === id);
    if (todo) {
      // this.log(`fetched todo ${id}`);
      deferred.resolve(todo);
    } else {
      const error = `todo with id=${id} not found`;
      // this.log(error);
      deferred.reject(error);
    }
    return deferred.promise;
  }

  /* GET todo-list whose name contains search term */
  searchTodos(term: string): IPromise<Todo[]> {
    const deferred = this.$q.defer<Todo[]>();
    const error = `no todos with name that contains ${term}`;
    if (!term.trim()) {
      // if not search term, return empty hero array.
      // this.log(error);
      deferred.resolve([]);
    }
    const todos = this.todos.filter(todo => todo.name.includes(term));
    // this.log(`found ${todo-list.length} todo-list whose name contains ${term}`);
    deferred.resolve(todos);
    return deferred.promise;
  }

  //////// Save methods //////////

  /** POST: add a new todo to the server */
  addTodo(name: string): IPromise<Todo> {
    const deferred = this.$q.defer<Todo>();
    const todo = {
      name,
      done: false,
      description: "",
      id: Date.now()
    };
    // this.todo-list.push(todo);
    // this.log('added new todo');

    this.$http({
      method: "POST",
      url: this.TODO_API,
      data: JSON.stringify(todo)
      // contentType: "application/json"
    }).then(response => {
      this.todos.push(todo);
      name = "";
      // this.list = response.data;
    });
    deferred.resolve(todo);
    return deferred.promise;
    // return this.$http.post<Todo>(this.heroesUrl, todo);
  }

  /** DELETE: delete the todo from the server */
  deleteTodo(index): IPromise<boolean> {
    const deferred = this.$q.defer<boolean>();
    const itemIndex = { index: index };
    // this.todo-list.splice(index, 1);
    // this.log(`deleted hero with id=${id}`);

    this.$http
      .delete(this.TODO_API, {
        data: itemIndex,
        headers: { "Content-Type": "application/json;charset=utf-8" }
      })
      .then(response => {
        this.todos.splice(index, 1);
      });

    deferred.resolve(true);

    return deferred.promise;
    // return this.$http.delete<Todo>(url);
  }

  /** PUT: update the todo on the server */
  updateTodo(todo): IPromise<Todo> {
    const deferred = this.$q.defer<Todo>();
    const foundIndex = this.todos.findIndex(x => x.id == todo.id);
    this.todos[foundIndex] = todo;
    todo.registrationId = this.pushService.deviceId;
    if (foundIndex > -1) {
      this.$http<Todo[]>({
        method: "PATCH",
        url: this.TODO_API,
        data: JSON.stringify(todo)
        // contentType: "application/json"
      }).then(response => {
        // this.todo-list = response.data;
      });

      // this.log(`updated todo with id=${todo.id}`);
      deferred.resolve(todo);
    } else {
      const error = `failed to update todo with id=${todo.id}, not found`;
      // this.log(error);
      deferred.reject(error);
    }
    return deferred.promise;
    // return this.$http.put(this.heroesUrl, todo);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  // private handleError<T> (operation = 'operation', result?: T) {
  //   return (error: any): Observable<T> => {
  //
  //     // TODO: send the error to remote logging infrastructure
  //     console.error(error); // log to console instead
  //
  //     // TODO: better job of transforming error for user consumption
  //     this.log(`${operation} failed: ${error.message}`);
  //
  //     // Let the app keep running by returning an empty result.
  //     return of(result as T);
  //   };
  // }

  /** Log a TodoService message with the MessageService */
  // private log(message: string) {
  //   this.messageService.add(`TodoService: ${message}`);
  // }

  private getNewId() {
    if (!this.todos.length) {
      return 1;
    }
    return Math.max(...this.todos.map(h => h.id)) + 1;
  }
}
