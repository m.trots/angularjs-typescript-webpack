import { Injectable } from "angular-ts-decorators";
import { StateService } from "@uirouter/angularjs";

@Injectable("pushService")
export class PushService {
  public deviceId = "";

  /*@ngInject*/
  constructor(private $state: StateService) {}

  registartion() {
    const push = PushNotification.init({
      android: {}
    });

    push.on("registration", data => {
      console.log(data);
      console.log(data.registrationId);
      this.deviceId = data.registrationId;
    });

    push.on("notification", data => {
      console.log("data", data);

      this.$state.go("detail", { id: data.additionalData.identifierNumber });
    });

    push.on("error", e => {
      console.log(e.message);
    });
  }
}
