import { StateService } from '@uirouter/angularjs';
import { IHttpResponse } from 'angular';
import { Component, Input, OnInit } from 'angular-ts-decorators';
import { Todo } from '../todo';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-detail',
  template: require('./todo-detail.component.html'),
  // styles: [ require('./todo-detail.component.scss') ]
})
export class TodoDetailComponent implements OnInit {
  @Input() todo: Todo;

  /*@ngInject*/
  constructor(
    private $state: StateService,
    private todoService: TodoService
  ) {}

  ngOnInit(): void {
    this.getTodo();
  }

  getTodo(): void {
    const id:number = +this.$state.params.id;
    this.todoService.getTodo(id)
      .then(todo => this.todo = todo);
  }

  goBack(): void {
    this.$state.go('list-todo');
  }

 save(): void {
    this.todoService.updateTodo(this.todo)
      .then(() => this.goBack());
  }
}
