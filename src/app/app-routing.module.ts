import uiRouter from "@uirouter/angularjs";
import { StateProvider } from "@uirouter/angularjs";

import { Ng1StateDeclaration } from "@uirouter/angularjs/lib/interface";
import { getTypeName, NgModule } from "angular-ts-decorators";
import { TodoDetailComponent } from "./todo-detail/todo-detail.component";
import { TodoListComponent } from "./todo-list/todo-list.component";

export interface UiState extends Ng1StateDeclaration {
  component?: any;
}

const routes: UiState[] = [
  { name: "index", url: "", redirectTo: "list-todo" },
  { name: "detail", url: "/detail/{id}", component: TodoDetailComponent },
  { name: "list-todo", url: "/list-todo", component: TodoListComponent }
];

@NgModule({
  id: "AppRoutingModule",
  imports: [uiRouter]
})
export class AppRoutingModule {
  static config($stateProvider: StateProvider) {
    "ngInject";
    routes.forEach(route =>
      $stateProvider.state(getNg1StateDeclaration(route))
    );
  }
}

function getNg1StateDeclaration(state: UiState) {
  if (state.component && typeof state.component !== "string") {
    state.component = getTypeName(state.component);
  }
  return state;
}
