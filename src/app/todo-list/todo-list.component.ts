import { Component, OnInit } from "angular-ts-decorators";
import { Todo } from "../todo";
import { TodoService } from "../todo.service";
import { StateService } from '@uirouter/angularjs';

@Component({
  selector: "app-todo-list",
  template: require("./todo-list.component.html")
  // styles: [require('./todo-list.component.scss')]
})
export class TodoListComponent implements OnInit {
  todos: Todo[];
  title = "Task List";

  /*@ngInject*/
  constructor(private todoService: TodoService,
              private $state: StateService) {}

  ngOnInit() {
    this.getTodos();
  }

  refreshPage(){
    window.location.reload();
  }

  getTodos(): void {
    this.todoService.getTodos().then(todos => (this.todos = todos));
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.todoService.addTodo(name).then(() => this.getTodos());
  }

  delete($index): void {
    // this.todo-list = this.todo-list.filter(h => h !== todo);
    this.todoService.deleteTodo($index).then();
  }

  changeIsDone(todo, id): void {
    console.log(todo);
    console.log(id);
    if (todo.done == true){
      todo.done = false;
      this.todoService.updateTodo(todo)
        .then(() => this.getTodos());
    } else todo.done = true;
    this.todoService.updateTodo(todo)
      .then(() => this.getTodos());


  }
}
