// const webpack = require('webpack');
// const HtmlWebPackPlugin = require('html-webpack-plugin');
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
//
// const path = require('path');
//
// const sourcePath = path.resolve(__dirname, 'src');
// const distPath = path.resolve(__dirname, 'dist');
//
// module.exports = (env, argv) => {
//   const isProd = argv.mode === 'production';
//
//   const plugins = [
//     new HtmlWebPackPlugin({
//       template: sourcePath + '/index.html'
//     }),
//     new MiniCssExtractPlugin({
//       filename: '[name].[contenthash:4].css',
//       chunkFilename: '[id].[contenthash:4].css'
//     }),
//     new ForkTsCheckerWebpackPlugin({
//       tslint: true,
//       checkSyntacticErrors: true
//     })
//   ];
//
//   if (isProd) {
//     plugins.push(
//       new webpack.NormalModuleReplacementPlugin(
//         /\/environments\/environment\.ts/,  `${sourcePath}/environments/environment.prod.ts`
//       ),
//       new UglifyJsPlugin({ sourceMap: true })
//     );
//   } else {
//     plugins.push(new webpack.NamedModulesPlugin(), new webpack.HotModuleReplacementPlugin());
//   }
//
//   const config = {
//     entry: {
//       app: sourcePath + '/main.ts',
//     },
//     output: {
//       path: distPath,
//       filename: '[name].bundle.[hash:4].js',
//     },
//     module: {
//       rules: [
//         {
//           test: /\.html$/,
//           loader: 'html-loader',
//           options: { minimize: true }
//         },
//         {
//           test: /\.css$/,
//           use: [MiniCssExtractPlugin.loader, 'css-loader', 'resolve-url-loader']
//         },
//         {
//           test: /\.scss$/,
//           use: [MiniCssExtractPlugin.loader, 'css-loader', 'resolve-url-loader', 'sass-loader']
//         },
//         {
//           test: /\.ts$/,
//           exclude: /node_modules/,
//           use: [
//             {
//               loader: 'ng-annotate-loader',
//               options: {
//                 ngAnnotate: 'ng-annotate-patched',
//                 sourcemap: !isProd,
//               },
//             },
//             {
//               loader: 'ts-loader',
//               options: {
//                 configFile: sourcePath + '/tsconfig.app.json',
//                 // disable type checker - we will use it in fork plugin
//                 transpileOnly: true,
//               }
//             }
//           ]
//         },
//         {
//           test: /\.(gif|png|jpe?g|svg)$/i,
//           loader: 'file-loader',
//           options: {
//             name: 'images/[name].[ext]'
//           }
//         },
//         {
//           test: /\.(eot|ttf|woff|woff2)$/,
//           loader: 'file-loader',
//           options: {
//             name: 'fonts/[name].[ext]'
//           }
//         }
//       ],
//     },
//     resolve: {
//       extensions: ['.js', '.ts'],
//       modules: [
//         path.resolve(__dirname, 'node_modules'),
//         sourcePath
//       ]
//     },
//     plugins,
//     optimization: {
//       splitChunks: {
//         cacheGroups: {
//           commons: {
//             test: /[\\/]node_modules[\\/]/,
//             name: 'vendors',
//             chunks: 'all'
//           }
//         }
//       }
//     },
//     // devtool: 'eval-source-map',
//     devServer: {
//       contentBase: distPath,
//       hot: true
//     }
//   };
//
//   if (!isProd) {
//     config.devtool = 'source-map';
//   }
//
//   return config;
// };
// module.exports = {
//   mode: "development",
//   devtool: "inline-source-map",
//   entry: "./src/main.ts",
//   output: {
//     filename: "../src/dist/bundle.js"
//   },
//   resolve: {
//     // Add `.ts` and `.tsx` as a resolvable extension.
//     extensions: [".ts", ".tsx", ".js"]
//   },
//   module: {
//     rules: [
//       // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      // {
      //   test: /\.html$/,
      //   use: [
      //     {
      //       loader: "html-loader"
      //     }
      //   ]
      // },
      // {
      //   test: /\.ts$/,
      //   use: [
      //     {
      //       loader: "ng-annotate-loader",
      //       options: {
      //         ngAnnotate: "ng-annotate-patched",
      //         dynamicImport: true,
      //         sourcemap: true
      //       }
      //     },
      //     { loader: "ts-loader" }
      //   ]

        // test: /\.ts$/,
        // use: [
        //   {
        //     loader: "ts-loader"
        //   }
        // ]
//       }
//     ]
//   }
// };


//потрібно щоб коректно відображалась файлова система
const path = require("path");
const TerserPlugin = require("terser-webpack-plugin");
module.exports = {
  entry: {
    bundle: "./src/main.ts",
    // vendor: ["moment", "lodash"]
  },
  output: {
    //пишем в квадратних дужках щоб створювати відповідні скомпільовані файли
    filename: "[name].js",
    //створюєм в корені папку dist
    path: path.resolve(__dirname, "./src/dist"),
    //Щоб коректно працював дев режим, можем не вказувати уже метод path просто задаєм папку
    publicPath: "/dist"
  },
  devServer: {
    // щоб відображати помилки на екрані на чорному фоні червоними літерами
    overlay: true
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".tsx", ".js"]
  },
  optimization: {
    // minimize: true,
    minimizer: [
      new TerserPlugin({
        sourceMap: true // Must be set to true if using source-maps in production
      })
    ]
  },
  module: {
    rules: [
      // HTML-Loader
      {
        test: /\.html$/,
        use: [{ loader: "html-loader" }]
      },
      // TS-Loader
      {
        test: /\.ts$/,
        use: [
          {
            loader: "ng-annotate-loader",
            options: {
              ngAnnotate: "ng-annotate-patched",
              dynamicImport: true,
              sourcemap: true
            }
          },
          { loader: "ts-loader" }
        ]
      }
    ]
  },
  devtool: "source-map"
};
